#ifndef LAB7_TIMESTAMP_H
#define LAB7_TIMESTAMP_H

#include <time.h>

#include "errhandle.h"


long timespecToMs(struct timespec ts);

struct timespec msToTimespec(long timeMs);

int setTimeRealtimeMs(long timeMs);

int getTimeRealtimeMs(long *result);

int getResolutionRealtimeMs(long *result);

#endif //LAB7_TIMESTAMP_H
