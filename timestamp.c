#include "timestamp.h"

#include <string.h>
#include <errno.h>


long timespecToMs(const struct timespec ts) {
    const time_t sec = ts.tv_sec;
    const long msec = ts.tv_nsec / 1000000;
    return sec * 1000 + msec;
}


struct timespec msToTimespec(long timeMs) {
    const time_t sec = (time_t) (timeMs / 1000);
    const long nsec = (timeMs % (sec * 1000)) * 1000 * 1000;
    return (struct timespec) {
            .tv_sec = timeMs / 1000,
            .tv_nsec = nsec
    };
}


int setTimeRealtimeMs(long timeMs) {
    const struct timespec time = msToTimespec(timeMs);
    int errcode = clock_settime(CLOCK_REALTIME, &time);
    if (NO_ERROR != errcode) {
        errorfln("Cannot set time: %s", strerror(errno));
        return errcode;
    }
    return NO_ERROR;
}


int getTimeRealtimeMs(long *const result) {
    if (NULL == result) {
        errorfln("%s: result pointer in NULL", __FUNCTION__);
        return EINVAL;
    }
    struct timespec timestamp;
    int errcode = clock_gettime(CLOCK_REALTIME, &timestamp);
    if (NO_ERROR != errcode) {
        errorfln("Cannot obtain time: %s", strerror(errno));
        return errcode;
    }
    *result = timespecToMs(timestamp);
    return NO_ERROR;
}


int getResolutionRealtimeMs(long *const result) {
    if (NULL == result) {
        errorfln("%s: result pointer in NULL", __FUNCTION__);
        return EINVAL;
    }
    struct timespec timestamp;
    int errcode = clock_getres(CLOCK_REALTIME, &timestamp);
    if (NO_ERROR != errcode) {
        errorfln("Cannot obtain precision: %s", strerror(errno));
        return errcode;
    }
    *result = timespecToMs(timestamp);
    return NO_ERROR;
}
