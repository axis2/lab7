#ifndef LAB8_RANGE_H
#define LAB8_RANGE_H

/**
 * Describes a range [start, end) to be iterated over with step step.
 */
struct st_Range {
    unsigned long long start;
    unsigned long long end;
    unsigned long long step;
};
typedef struct st_Range Range_t;

struct st_UnlimitedRange {
    unsigned long long start;
    unsigned long long step;
};
typedef struct st_UnlimitedRange UnlimitedRange_t;

#endif //LAB8_RANGE_H
