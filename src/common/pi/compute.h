#ifndef LAB8_COMPUTE_H
#define LAB8_COMPUTE_H

#include "../range.h"

/**
 * Number of iterations for Leibniz's formula divided by 2.
 */
#define NUM_STEPS       200000000u

double computePi(Range_t range);

#endif //LAB8_COMPUTE_H
