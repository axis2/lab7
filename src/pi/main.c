#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <stdbool.h>
#include <errno.h>
#include <limits.h>
#include <signal.h>

#include "../common/range.h"
#include "../common/pi/compute.h"
#include "../common/errhandle.h"
#include "argparse/argparse.h"



struct st_PartialSumArgs {
    bool signal_checker_thread;
    UnlimitedRange_t range;
    double *p_result;
    pthread_barrier_t *p_barrier;
};
typedef struct st_PartialSumArgs PartialSumArgs_t;


#define CHECK_STEPS     (NUM_STEPS / 100)


static bool must_stop = false;
static bool sigint_received = false;

/**
 * Compute partial sum of pi / 4.
 * @param v_arg pointer to [PartialSumArgs_t] object
 * @return pointer to computed partial sum or [NULL] if an error occurred
 */
void *computePartialSum(void *v_arg) {
    if (NULL == v_arg) {
        errorfln("[%s] Unexpected value for arguments pointer: %p", __FUNCTION__ , v_arg);
        return NULL;
    }

    PartialSumArgs_t *const args = (PartialSumArgs_t *) v_arg;
    if (NULL == args->p_result) {
        errorfln("Unexpected value for result pointer: %p", args->p_result);
        return NULL;
    }
    if (NULL == args->p_barrier) {
        errorfln("Unexpected value for barrier pointer: %p", args->p_barrier);
        return NULL;
    }

    const bool signal_checker_thread = args->signal_checker_thread;
    const UnlimitedRange_t range = args->range;
    double *const p_result = args->p_result;
    pthread_barrier_t *const p_barrier = args->p_barrier;

    double partial_sum = 0.0;

    for (long long unsigned i = 0;; i += 1) {
        partial_sum += computePi((Range_t) {
            .start = range.start + i * CHECK_STEPS,
            .end = range.start + (i + 1) * CHECK_STEPS,
            .step = range.step
        });

        if (signal_checker_thread && sigint_received) {
            must_stop = true;
        }

        int errcode = pthread_barrier_wait(p_barrier);
        if (PTHREAD_BARRIER_SERIAL_THREAD != errcode && NO_ERROR != errcode) {
            errorfln("Barrier wait failed: %s\nExiting", strerror(errcode));
            exit(EXIT_FAILURE);
        }
        if (must_stop) {
            if (signal_checker_thread) {
                printf("%llu iterations\n", CHECK_STEPS * i);
            }
            break;
        }
        errcode = pthread_barrier_wait(p_barrier);
        if (PTHREAD_BARRIER_SERIAL_THREAD != errcode && NO_ERROR != errcode) {
            errorfln("Barrier wait failed: %s\nExiting", strerror(errcode));
            exit(EXIT_FAILURE);
        }
    }

    *p_result = partial_sum;
    return (void *) p_result;
}

/**
 * Fill thread arguments array.
 * @param args arguments array
 * @param threads_count number of worker threads
 * @param partial_sums array of results
 */
int createArguments(
        PartialSumArgs_t *args,
        unsigned long threads_count,
        const double *const partial_sums,
        const pthread_barrier_t *const p_barrier) {
    if (NULL == args || NULL == partial_sums) {
        errorfln("[%s] Unexpected pointer argument value: %p", __FUNCTION__, NULL);
        return !NO_ERROR;
    }

    for (int i = 0; i < threads_count; i += 1) {
        args[i] = (PartialSumArgs_t) {
                .signal_checker_thread = (i == 0),
                .range = (UnlimitedRange_t) {
                        .start = i,
                        .step = threads_count
                },
                .p_result = (double *) &partial_sums[i],
                .p_barrier = (pthread_barrier_t *) p_barrier
        };
    }

    return NO_ERROR;
}

/**
 * Spawn threads to compute partial sums.
 * @param child_threads array of child thread descriptors
 * @param threads_count number of worker threads to spawn
 * @param args array of thread arguments
 * @return number of successfully spawned threads
 */
unsigned spawnComputingThreads(
        pthread_t *child_threads,
        unsigned long threads_count,
        PartialSumArgs_t *args) {
    if (NULL == child_threads || NULL == args) {
        errorfln("[%s] Unexpected pointer argument value: %p", __FUNCTION__, NULL);
        return 0;
    }

    unsigned spawned_threads_count = 0u;

    int errcode = NO_ERROR;

    for (int i = 0; i < threads_count; i += 1) {
        errcode = pthread_create(&child_threads[i], NULL, computePartialSum, &args[i]);
        if (NO_ERROR != errcode) {
            errorfln("Failed to spawn a thread: %s", strerror(errcode));
            break;
        }
        spawned_threads_count += 1;
    }

    return spawned_threads_count;
}

/**
 * Collect partial sums from worker threads.
 * @param child_threads array of child thread descriptors
 * @param threads_count number of worker threads to join
 * @param p_accumulator pointer to sum accumulator
 * @return [true] if all partial sums have successfully been collected and [false] if an error occurred
 */
bool collectPartialSums(
        pthread_t *child_threads,
        unsigned long threads_count,
        double *p_accumulator) {
    if (NULL == child_threads || NULL == p_accumulator) {
        errorfln("[%s] Unexpected pointer argument value: %p", __FUNCTION__, NULL);
        return false;
    }

    bool all_partial_sums_collected = true;
    int errcode = NO_ERROR;
    double accumulator = 0.0;

    for (int i = 0; i < threads_count; i += 1) {
        void *ret_val;

        errcode = pthread_join(child_threads[i], &ret_val);
        if (NO_ERROR != errcode) {
            errorfln("Failed to join child thread: %s", strerror(errcode));
            all_partial_sums_collected = false;
            continue;
        }

        if (NULL == ret_val) {
            all_partial_sums_collected = false;
            continue;
        }

        double *p_partial_sum = (double *) ret_val;
        accumulator += *p_partial_sum;
    }

    *p_accumulator = accumulator;
    return all_partial_sums_collected;
}


void setSignalReceived(int signal) {
    sigint_received = (SIGINT == signal);
}


int main(
        int argc,
        char **argv) {
    unsigned long threads_count;
    int errcode = getThreadsCount(argc, argv, &threads_count);
    if (NO_ERROR != errcode) {
        printUsage();
        return EXIT_FAILURE;
    }

    void (*prev)(int) = signal(SIGINT, setSignalReceived);
    if (SIG_ERR == prev) {
        errorfln("Failed to signal handler: %s", strerror(errno));
        return EXIT_FAILURE;
    }

    pthread_t child_threads[threads_count];
    double partial_sums[threads_count];
    PartialSumArgs_t args[threads_count];

    pthread_barrier_t barrier;
    errcode = pthread_barrier_init(&barrier, NULL, threads_count);
    if (NO_ERROR != errcode) {
        errorfln("Cannot initialize barrier: %s", strerror(errcode));
        return EXIT_FAILURE;
    }

    errcode = createArguments(args, threads_count, partial_sums, &barrier);
    if (NO_ERROR != errcode) {
        return EXIT_FAILURE;
    }

    const unsigned spawned_threads_count = spawnComputingThreads(child_threads, threads_count, args);
    if (spawned_threads_count != threads_count) {
        errorfln("Failed to spawn requested number of threads (%lu)\nExiting", threads_count);
        return EXIT_FAILURE;
    }

    double pi_div_4 = 0.0;
    const bool collected_from_spawned_threads = collectPartialSums(child_threads, spawned_threads_count, &pi_div_4);

    errcode = pthread_barrier_destroy(&barrier);
    if (NO_ERROR != errcode) {
        errorfln("Failed to destroy barrier: %s", strerror(errcode));
    }

    if (!collected_from_spawned_threads || spawned_threads_count != threads_count) {
        errorfln("Failed to collect partial sums from %lu child threads", threads_count);
        return EXIT_FAILURE;
    }

    printf("Pi = %.15lf\n", pi_div_4 * 4.0);

    return EXIT_SUCCESS;
}
