#include <stdio.h>
#include <limits.h>
#include <stdlib.h>

#include "../common/errhandle.h"
#include "../common/timestamp/timestamp.h"
#include "../common/range.h"
#include "../common/pi/compute.h"


#define THREADS_MAX     500u


#define MILLISECOND 1llu
#define SECOND      (1000llu * MILLISECOND)
#define MINUTE      (60llu * SECOND)
#define HOUR        (60llu * MINUTE)
#define DAY         (24llu * HOUR)
#define YEAR        (365llu * DAY)


#define TIME_UNIT_NAME(TimeUnit) \
    ((TimeUnit) == YEAR \
        ? "year" \
        : ((TimeUnit) == DAY \
            ? "day" \
            : ((TimeUnit) == HOUR \
                ? "hour" \
                : ((TimeUnit) == MINUTE \
                    ? "minute" \
                    : ((TimeUnit) == SECOND \
                        ? "second" \
                        : ((TimeUnit) == MILLISECOND \
                            ? "millisecond" \
                            : "unknown"))))))


void printTime(unsigned long long timeMs) {
    const unsigned long long denominators[] = {
            YEAR,
            DAY,
            HOUR,
            MINUTE,
            SECOND,
            MILLISECOND
    };

    for (int i = 0; i + 1 < sizeof(denominators) / sizeof(*denominators); i += 1) {
        if (timeMs > denominators[i]) {
            printf(
                    "Approximately %llu %ss %llu %ss\n",
                    timeMs / denominators[i], TIME_UNIT_NAME(denominators[i]),
                    (timeMs % denominators[i]) / denominators[i + 1], TIME_UNIT_NAME(denominators[i + 1]));
            return;
        }
    }

    printf("%llu %ss\n", timeMs, TIME_UNIT_NAME(MILLISECOND));
}


int main(void) {
    const unsigned long long niter = NUM_STEPS;

    printf("Will do %llu iterations\n", niter);

    long start_time_ms;
    long end_time_ms;

    int errcode = getTimeMs(CLOCK_MONOTONIC, &start_time_ms);
    if (NO_ERROR != errcode) {
        return EXIT_FAILURE;
    }

    const double pi_div_4 = computePi((Range_t) { .start = 0, .end = niter, .step = 1 });

    errcode = getTimeMs(CLOCK_MONOTONIC, &end_time_ms);
    if (NO_ERROR != errcode) {
        return EXIT_FAILURE;
    }

    printf("pi = %lf\n", pi_div_4 * 4.0); // so compiler does not optimize computePi out of my code

    const unsigned long time_taken_ms = end_time_ms - start_time_ms;

    printf("Single thread, %llu iterations:\n", niter);
    printTime(time_taken_ms);

    const unsigned long long time_taken_s = time_taken_ms / SECOND;
    const unsigned long long llu_iter_count = ULLONG_MAX;
    const unsigned long long iter_per_thread = llu_iter_count / THREADS_MAX;
    const unsigned long long approximate_total_time_s = time_taken_s * (iter_per_thread / niter);

    printf("%u threads, %llu iterations total, %llu per thread:\n", THREADS_MAX, llu_iter_count, iter_per_thread);
    printTime(approximate_total_time_s * SECOND);
}
